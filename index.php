<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="shortcut icon" href="#">
  <title>API test</title>
</head>
<body>

  <div id="Chuck Norris"></div>
  <button id="reload_chuck">Reload</button>
  <br>
  <div style="display: flex;">
    <div id="Dog"></div>
    <div id="Cat"></div>
  </div>
  
  <script src="script.js"></script>
</body>
</html>