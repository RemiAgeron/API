fetch('https://api.chucknorris.io/jokes/random')
.then(response => response.json())
.then(function(data) {
  var div = document.getElementById("Chuck Norris")

  var p = document.createElement("p")
  p.id = "joke"
  p.appendChild(document.createTextNode(data['value']))
  div.appendChild(p)

  var img = document.createElement("img")
  img.setAttribute("src", data['icon_url'])
  div.style = "margin: 20px;"
  div.appendChild(img)
})

fetch('https://dog.ceo/api/breeds/image/random')
.then(function(response) {
  return response.json()
})
.then(function(data) {
  var div = document.getElementById("Dog")

  var img = document.createElement("img")
  img.src = data.message
  img.id = "dog"
  img.style = "height: 250px; margin: 0 20px;"
  div.appendChild(img)
})

fetch('https://thatcopy.pw/catapi/rest/')
.then(function(response) {
  return response.json();
})
.then(function(data) {
  var div = document.getElementById("Cat")

  var img = document.createElement("img")
  img.src = data['url']
  img.id = "cat"
  img.style = "height: 250px; margin: 0 20px;"
  div.appendChild(img)
})

const btn_chuck = document.getElementById("reload_chuck")
btn_chuck.addEventListener('click', () => {
  fetch('https://api.chucknorris.io/jokes/random')
  .then(response => response.json())
  .then(data => {
    const p = document.getElementById("joke")
    p.textContent = data.value
  })
})